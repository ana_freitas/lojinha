package edu.ifsp.lojinha.controlador.cliente;

import java.io.IOException;

import edu.ifsp.lojinha.controlador.Command;
import edu.ifsp.lojinha.controlador.Forward;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

@WebServlet("/cadastrar")
public class CadastroController extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		final String cmd = request.getParameter("cmd");
		Command command = null;
		if ("exists".equals(cmd)) {
			command = new CheckUsername();
		} else if ("detalhar".equals(cmd)) {
			command = new DetalharCliente();
		} else {
			command = new Forward("cadastro.jsp");
		}

		command.processar(request, response);		
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Command command = new SalvarCliente();
		command.processar(request, response);		
	}

}
