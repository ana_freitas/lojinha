package edu.ifsp.lojinha.controlador.cliente;

import java.io.IOException;

import edu.ifsp.lojinha.controlador.Command;
import edu.ifsp.lojinha.persistencia.PersistenceException;
import edu.ifsp.lojinha.persistencia.UsuarioDAO;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

public class CheckUsername implements Command {

	@Override
	public void processar(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		final String username = request.getParameter("username");
		UsuarioDAO usuarioDao = new UsuarioDAO();
		try {
			boolean exists = usuarioDao.exists(username);
			response.setContentType("text/plain");
			response.getWriter().print(exists);
		} catch (PersistenceException e) {
			throw new ServletException(e);
		}		
	}

}
