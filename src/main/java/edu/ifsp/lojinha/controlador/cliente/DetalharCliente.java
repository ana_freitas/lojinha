package edu.ifsp.lojinha.controlador.cliente;

import java.io.IOException;

import edu.ifsp.lojinha.controlador.Command;
import edu.ifsp.lojinha.modelo.Cliente;
import edu.ifsp.lojinha.persistencia.ClienteDAO;
import edu.ifsp.lojinha.persistencia.PersistenceException;
import edu.ifsp.lojinha.web.FlashUtil;
import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

public class DetalharCliente implements Command {

	@Override
	public void processar(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		int idCliente = Integer.valueOf(request.getParameter("id"));		
		ClienteDAO dao = new ClienteDAO();
		try {
			Cliente cliente = dao.findById(idCliente);
			if (cliente != null) {
				request.setAttribute("cliente", cliente);
				request.setAttribute("usuario", cliente.getUsuario());
			} else {
				request.setAttribute("notfound", true);
			}

		} catch (PersistenceException e) {
			throw new ServletException(e);
		}		
		
		
		FlashUtil.move(request, "cliente:salvar");
		
		/* gerando a página de resposta */
		RequestDispatcher rd = request.getRequestDispatcher("cadastro.jsp");
		rd.forward(request, response);		
	}

}
