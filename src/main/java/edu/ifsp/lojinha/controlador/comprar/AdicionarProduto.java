package edu.ifsp.lojinha.controlador.comprar;

import java.io.IOException;
import java.util.List;

import edu.ifsp.lojinha.controlador.Command;
import edu.ifsp.lojinha.modelo.Produto;
import edu.ifsp.lojinha.persistencia.ProdutoDAO;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

public class AdicionarProduto implements Command {
	private ProdutoDAO produtoDao = new ProdutoDAO();

	@SuppressWarnings("unchecked")
	@Override
	public void processar(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		HttpSession session = request.getSession();
		List<Produto> carrinho = (List<Produto>)session.getAttribute("carrinho");		
		
		String paramProduto = request.getParameter("produto");
		int id = Integer.parseInt(paramProduto);
		Produto produto = produtoDao.findById(id);
		carrinho.add(produto);	

		response.sendRedirect("comprar.sec");
	}
}
