package edu.ifsp.lojinha.controlador.comprar;

import edu.ifsp.lojinha.controlador.Command;

/**
 * Representa uma fábrica ("factory") de objetos do tipo {@code Command}.
 * 
 * O uso desta classe não representa uma aplicação do padrão Factory Method [GoF],
 * que tem como característica principal "permitir a uma classe delegar a 
 * instanciação de objetos concretos a suas subclasses" [GoF].  Confundir uma
 * classe Factory, como a que foi usada neste projeto, com a aplicação do padrão
 * Factory Method é um erro bastante comum.
 */
public class Factory {
	public static Command getCommand(String cmd) {		
		Command command = null;		

		if ("add".equals(cmd)) {			
			command = new AdicionarProduto();
		} else if ("esvaziar".equals(cmd)) {
			command = new EsvaziarCarrinho();
		} else {
			command = new IniciarCompra();
		}
		return command;
	}
}
