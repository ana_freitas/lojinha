package edu.ifsp.lojinha.controlador.comprar;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import edu.ifsp.lojinha.controlador.Command;
import edu.ifsp.lojinha.modelo.Produto;
import edu.ifsp.lojinha.persistencia.ProdutoDAO;
import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

public class IniciarCompra implements Command {
	private ProdutoDAO produtoDao = new ProdutoDAO();

	@SuppressWarnings("unchecked")
	@Override
	public void processar(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		HttpSession session = request.getSession();
		List<Produto> carrinho = (List<Produto>)session.getAttribute("carrinho");		
		if (carrinho == null) {
			carrinho = new ArrayList<Produto>();
			session.setAttribute("carrinho", carrinho);
		}

		List<Produto> produtos = produtoDao.listarTodos();
		request.setAttribute("produtos", produtos);
		
		RequestDispatcher rd = request.getRequestDispatcher("comprar.jsp");
		rd.forward(request, response);		
	}
}
