package edu.ifsp.lojinha.controlador.produto;

import java.io.IOException;

import edu.ifsp.lojinha.controlador.Command;
import edu.ifsp.lojinha.persistencia.PersistenceException;
import edu.ifsp.lojinha.persistencia.ProdutoDAO;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

public class CheckProduto implements Command {

	@Override
	public void processar(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		final String descricao = request.getParameter("descricao");
		ProdutoDAO produtoDao = new ProdutoDAO();
		try {
			boolean exists = produtoDao.exists(descricao);
			response.setContentType("text/plain");
			response.getWriter().print(exists);
		} catch (PersistenceException e) {
			throw new ServletException(e);
		}		
	}

}