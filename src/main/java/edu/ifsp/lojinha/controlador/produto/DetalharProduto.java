package edu.ifsp.lojinha.controlador.produto;

import java.io.IOException;

import edu.ifsp.lojinha.controlador.Command;
import edu.ifsp.lojinha.modelo.Produto;
import edu.ifsp.lojinha.persistencia.ProdutoDAO;
import edu.ifsp.lojinha.web.FlashUtil;
import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

public class DetalharProduto implements Command{
	
	public void processar(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		int idProduto = Integer.valueOf(request.getParameter("id"));		
		ProdutoDAO dao = new ProdutoDAO();
		Produto produto = dao.findById(idProduto);
		if (produto != null) {
			request.setAttribute("produto", produto);
		} else {
			request.setAttribute("notfound", true);
		}		
		
		FlashUtil.move(request, "produto:salvar");
		
		/* gerando a página de resposta */
		RequestDispatcher rd = request.getRequestDispatcher("produto.jsp");
		rd.forward(request, response);		
	}

}
