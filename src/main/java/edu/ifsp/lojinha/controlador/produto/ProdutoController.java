package edu.ifsp.lojinha.controlador.produto;

import java.io.IOException;

import edu.ifsp.lojinha.controlador.Command;
import edu.ifsp.lojinha.controlador.Forward;
import edu.ifsp.lojinha.controlador.cliente.CheckUsername;
import edu.ifsp.lojinha.controlador.cliente.DetalharCliente;
import edu.ifsp.lojinha.controlador.cliente.SalvarCliente;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

@WebServlet("/produto")
public class ProdutoController extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		final String cmd = request.getParameter("cmd");
		Command command = null;
		if ("exists".equals(cmd)) {
			command = new CheckProduto();
		} else if ("detalhar".equals(cmd)) {
			command = new DetalharProduto();
		} else {
			command = new Forward("produto.jsp");
		}

		command.processar(request, response);	
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Command command = new SalvarProduto();
		command.processar(request, response);	
	}
}
