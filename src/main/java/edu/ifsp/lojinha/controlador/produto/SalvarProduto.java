package edu.ifsp.lojinha.controlador.produto;

import java.io.IOException;
import java.math.BigDecimal;

import edu.ifsp.lojinha.controlador.Command;
import edu.ifsp.lojinha.modelo.Produto;
import edu.ifsp.lojinha.persistencia.PersistenceException;
import edu.ifsp.lojinha.persistencia.ProdutoDAO;
import edu.ifsp.lojinha.web.FlashUtil;
import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

public class SalvarProduto  implements Command {

	@Override
	public void processar(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String descricao = request.getParameter("descricao");
		BigDecimal preco = new BigDecimal(request.getParameter("preco").replace(",", "."));
		
		Produto prod = new Produto(0, descricao);
		prod.setPreco(preco);
		
		try {
			ProdutoDAO produtoDao = new ProdutoDAO();		
			if (!produtoDao.exists(descricao)) {
				produtoDao.save(prod);
				
				FlashUtil.setAttribute(request, "produto:salvar", "saved", true);
				response.sendRedirect("produto?cmd=detalhar&id=" + prod.getId());
			}else {
				request.setAttribute("produtoExists", true);
				RequestDispatcher rd = request.getRequestDispatcher("produto.jsp");
				rd.forward(request, response);
			}
		} catch (PersistenceException e) {
			throw new ServletException(e);
		}
	}

}
