package edu.ifsp.lojinha.persistencia;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import edu.ifsp.lojinha.modelo.Cliente;
import edu.ifsp.lojinha.modelo.Produto;

public class ProdutoDAO {
	
	private static String MISSING_GENERATED_KEY = "ID gerado n�o foi recuperado. [tabela: %s]";
	List<Produto> produtos = List.of(
			new Produto(1, "Batata"), 
			new Produto(2, "Cebola"), 
			new Produto(3, "Alho")
			);

	public List<Produto> listarTodos() {
		return produtos;
	}

	public Produto findById(int id) {
		for (Produto p : produtos) {
			if (p.getId() == id) {
				return p;
			}
		}
		
		return null;
	}
	
	public boolean exists(String descricao) throws PersistenceException {
		boolean found = false;
		
		try (Connection conn = DatabaseConnector.getConnection()) {			
			PreparedStatement ps = conn.prepareStatement(
					"SELECT count(*) FROM produto WHERE descricao = ?;");
			ps.setString(1, descricao);
			ResultSet rs = ps.executeQuery();
			
			if (rs.next() && rs.getInt(1) > 0) {				
				found = true;
			}
			
			rs.close();
			ps.close();
			
		} catch (SQLException e) {
			throw new PersistenceException(e);
		}
		
		return found;
	}
	
	public Produto save(Produto produto) throws PersistenceException {
		try (Connection conn = DatabaseConnector.getConnection()) {
			/* iniciando transação */
			conn.setAutoCommit(false);
			try {
				PreparedStatement ps = conn.prepareStatement(
						"INSERT INTO produto (descricao, preco) VALUES (?, ?);", 
						Statement.RETURN_GENERATED_KEYS);
				ps.setString(1, produto.getDescricao());
				ps.setBigDecimal(2, produto.getPreco());
				ps.executeUpdate();
				ResultSet rs = ps.getGeneratedKeys();
				if (rs.next()) {
					produto.setId(rs.getInt(1));
				} else {
					conn.rollback();
					throw new PersistenceException(String.format(MISSING_GENERATED_KEY, "produto"));
				}
				ps.close();
				rs.close();
								
				conn.commit();
				
			} catch (SQLException e) {
				conn.rollback();
				throw e;
			}
			
		} catch (SQLException e) {
			throw new PersistenceException(e);
		}
		
		return produto;
	}
}
