package edu.ifsp.lojinha.web;

import java.util.HashMap;
import java.util.Map;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpSession;

public class FlashUtil {
	
	@SuppressWarnings("unchecked")
	public static void setAttribute(HttpServletRequest request, String scope, String key, Object value) {
		HttpSession session = request.getSession();
		Map<String, Object> map = (Map<String, Object>)session.getAttribute(scope);
		if (map == null) {
			map = new HashMap<String, Object>();
			session.setAttribute(scope, map);
		}
		
		map.put(key, value);
	}
	
	@SuppressWarnings("unchecked")
	public static void move(HttpServletRequest request, String scope) {
		HttpSession session = request.getSession();
		Map<String, Object> map = (Map<String, Object>)session.getAttribute(scope);

		if (map != null) {
			map.forEach((key, value) -> {
				request.setAttribute(key, value);
			});
		}
		session.removeAttribute(scope);		
	}
}
