<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Cadastro de Clientes</title>
<link rel="stylesheet" href="css/cadastro.css">
<script type="text/javascript" src="js/cadastro.js"></script>
</head>
<body>

<h1>Cadastro de Clientes</h1>

<c:if test="${requestScope.saved}">
<p><span class="success flash">Dados salvos!</span></p>
</c:if>

<c:if test="${requestScope.notfound}">
<p><span class="warning">Cliente não encontrado!</span></p>
</c:if>

<form action="cadastrar" method="post">
	<p>
		<label for="username">Nome de usuário: </label>
		<input type="text" id="username" name="username" width="20" maxlength="20" 
			value="${requestScope.usuario.username}" required autocomplete="username" onchange="checkUser(this);">
		<c:choose>
			<c:when test="${requestScope.userExists}">
			<span class="error-msg">Este nome já está em uso. Por favor, escolha outro.</span>							
			</c:when>
			
			<c:otherwise>
			<span class="error-msg oculto">Este nome já está em uso. Por favor, escolha outro.</span>
			</c:otherwise>
		</c:choose>
	</p>

	<p>
		<label for="password">Escolha uma senha:</label>
		<input type="password" id="password" name="password" width="10" required autocomplete="new-password" onchange="checkPassword();">
	</p>

	<p>
		<label for="password-confirm">Confirme a senha:</label>
		<input type="password" id="password-confirm" name="password-confirm" width="10" required autocomplete="new-password" onchange="checkPassword();">
		<span class="error-msg oculto">As senhas não correspondem.</span>
	</p>

	<p>
		<label for="nome">Nome: </label>
		<input type="text" id="nome" name="nome" width="40" required maxlength="40" value="${requestScope.cliente.nome}" onchange="checkName();">
		<span class="error-msg oculto">Informe um nome válido.</span>
	</p>

	<p>	
		<label>E-mail: </label> 
		<input type="email" id="email" name="email" value="${requestScope.cliente.email}" required>
	</p>
	
	<br>
	<button type="submit">Salvar</button>
</form>

</body>
</html>