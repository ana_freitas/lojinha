<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Lista de Clientes</title>
<link rel="stylesheet" href="css/lojinha.css">

</head>
<body>
<h1>Escolha seus produtos</h1>


<c:choose>
	<c:when test="${empty sessionScope.carrinho}">
		<p>Carrinho vazio</p>
	</c:when>
	<c:otherwise>
		<p>${sessionScope.carrinho.size()} itens no <a href="carrinho.sec">carrinho</a> 
		<small>(<a href="comprar.sec?cmd=esvaziar" class="action">Esvaziar carrinho</a>)</small>
		</p>	
	</c:otherwise>
</c:choose>

<table border="1">
<tr>
	<th>Cód.</th>
	<th>Descrição</th>
	<th></th>
</tr>

<c:forEach items="${requestScope.produtos}" var="p">
	<tr>
		<td>${p.id}</td>
		<td>${p.descricao}</td>
		<td><small><a href="comprar.sec?cmd=add&produto=${p.id}" class="action">Adicionar</a></small></td>
	</tr>
</c:forEach>

</table>

</body>
</html>