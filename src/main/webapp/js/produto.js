function setErrorMessage(elementId, visible) {

	const selector = '#' + elementId + ' ~ .error-msg';
	let messageNode = document.querySelector(selector);	
	let inputElement = document.querySelector('input#' + elementId);

	if (visible) {
		messageNode.classList.remove('oculto');
		inputElement.setCustomValidity(messageNode.innerText);
	} else {
		messageNode.classList.add('oculto');
		inputElement.setCustomValidity('');
	}
}

function showErrorFor(elementId) {
	setErrorMessage(elementId, true);
}

function hideErrorFor(elementId) {
	setErrorMessage(elementId, false);
}

function checkProduto(inputElement) {
	let xhr = new XMLHttpRequest();
	xhr.onload = function() {
		if (xhr.responseText == 'true') {
			showErrorFor(inputElement.id);
			inputElement.focus();
		} else {
			hideErrorFor(inputElement.id);
		}
	};
	
	let descricao = inputElement.value;
	let url = 'produto?cmd=exists&username=' + username ;
	xhr.open('GET', url);
	xhr.send();
}

function scheduleAnimations() {
	document.addEventListener('DOMContentLoaded', function() {

		setTimeout(function() {
			 document.querySelectorAll('.flash')
				.forEach(node => node.classList.add('fade-out'));
		}, 1000);
	});
}
scheduleAnimations();
