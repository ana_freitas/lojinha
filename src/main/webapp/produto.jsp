<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>Cadastro de Produto</title>
		<link rel="stylesheet" href="css/cadastro.css">
	</head>
	<body>
	
		<h1>Cadastro de Produtos</h1>
		
		<c:if test="${requestScope.saved}">
			<p><span class="success flash">Dados salvos!</span></p>
		</c:if>
		
		<a href="home"> Voltar </a>
	
		<form action="produto" method="post">
			<p>
				<label for="descricao">Descrição do produto: </label>
				<input type="text" id="descricao" name="descricao" width="20"
					value="${requestScope.produto.descricao}" required autocomplete="descricao" onchange="checkProduto(this);">
				<c:choose>
					<c:when test="${requestScope.produtoExists}">
						<span class="error-msg">Este produto já existe.</span>							
					</c:when>
					
					<c:otherwise>
						<span class="error-msg oculto">Este produto já existe.</span>
					</c:otherwise>
				</c:choose>
			</p>
			
			<p>
				<label for="preco">Preço:</label>
				<input type="number" id="preco" name="preco" width="10" pattern="[0-9]+([\.,][0-9]+)?" step="0.01" required>
			</p>
			
			<button type="submit">Salvar</button>
		</form>
	</body>

</html>